webpackJsonp([4],{

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioPageModule", function() { return PortfolioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__portfolio__ = __webpack_require__(830);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PortfolioPageModule = /** @class */ (function () {
    function PortfolioPageModule() {
    }
    PortfolioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__portfolio__["a" /* PortfolioPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__portfolio__["a" /* PortfolioPage */]),
            ],
        })
    ], PortfolioPageModule);
    return PortfolioPageModule;
}());

//# sourceMappingURL=portfolio.module.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortfolioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PortfolioPage = /** @class */ (function () {
    function PortfolioPage(navCtrl, navParams, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.storage.get('btcCurrentBalancec').then(function (x) {
            _this.storage.get('ethCurrentBalancec').then(function (y) {
                _this.btcbal = x;
                _this.ethBal = y;
                _this.total = _this.btcbal + _this.ethBal;
                _this.date = new Date();
            });
        });
        console.log("this.total", this.total);
    }
    PortfolioPage.prototype.ionViewDidLoad = function () {
        this.allAddress();
    };
    PortfolioPage.prototype.allAddress = function () {
        var _this = this;
        this.storage.get('BtcAddress')
            .then(function (x) {
            _this.BtcAddress = x.address;
        });
        this.storage.get('Ethaddress').then(function (y) {
            _this.EthAddress = y.address;
        });
        this.storage.get('Xrpaddress').then(function (z) {
            _this.XrpAddress = z.address;
        });
    };
    PortfolioPage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    PortfolioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-portfolio',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/portfolio/portfolio.html"*/'<ion-content padding style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n\n  <div class="close-btn" (click)=back()>X</div>\n <div style="margin-top: 15px;">\n  <ion-card>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n      <ion-col col-7 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff;font-size: 12px; font-weight: 500">BTC</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:10px;">{{date | date : \'mediumDate\'}}</div>\n        </ion-row>\n        <ion-row>\n          <h6 style="color: #fff; margin-top: 9px; font-size: 12px;">Address</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:9px;">{{BtcAddress}}</div>\n        </ion-row>        \n      </ion-col>\n      <ion-col col-3 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff">Amount</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:8.7px;">INR {{btcbal | currency: \'INR\':true}}</div>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-card>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/Ripple-icon.png">\n      </ion-col>\n      <ion-col col-7 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff">XRP</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:10px;">{{date | date : \'mediumDate\'}}</div>\n        </ion-row>\n        <ion-row>\n          <h6 style="color: #fff; margin-top: 9px; font-size: 12px;">Address</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:9px;">{{XrpAddress}}</div>\n        </ion-row>  \n      </ion-col>\n      <ion-col col-3 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff">Amount</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:8.7px;">INR 0.00 </div>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-card>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/etherium_logo.png">\n      </ion-col>\n      <ion-col col-7 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff">ETH</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:10px;">{{date | date : \'mediumDate\'}}</div>\n        </ion-row>\n        <ion-row>\n          <h6 style="color: #fff; margin-top: 9px; font-size: 12px;">Address</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:9px;">{{EthAddress}}</div>\n        </ion-row>  \n      </ion-col>\n      <ion-col col-3 class="btc-top">\n        <ion-row>\n          <h6 style="color: #fff">Amount</h6>\n        </ion-row>\n        <ion-row>\n          <div style="font-size:8.7px;">INR {{ethBal | currency: \'INR\':true}}</div>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</div>\n \n      <ion-row>\n        <ion-col col-1>\n        \n        </ion-col>\n        <ion-col col-5 text-right>\n         \n          <h4 style="color: #fff">Total: </h4>\n         </ion-col>\n         <ion-col col-6 text-left>\n            <h4 style="color: #fff">{{total | currency: \'INR\':true}} </h4>\n         \n        </ion-col>\n     \n      </ion-row>\n    \n  \n  \n\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/portfolio/portfolio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], PortfolioPage);
    return PortfolioPage;
}());

//# sourceMappingURL=portfolio.js.map

/***/ })

});
//# sourceMappingURL=4.js.map