webpackJsonp([3],{

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestorekeyPageModule", function() { return RestorekeyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__restorekey__ = __webpack_require__(831);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RestorekeyPageModule = /** @class */ (function () {
    function RestorekeyPageModule() {
    }
    RestorekeyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__restorekey__["a" /* RestorekeyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__restorekey__["a" /* RestorekeyPage */]),
            ],
        })
    ], RestorekeyPageModule);
    return RestorekeyPageModule;
}());

//# sourceMappingURL=restorekey.module.js.map

/***/ }),

/***/ 831:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestorekeyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RestorekeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RestorekeyPage = /** @class */ (function () {
    function RestorekeyPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.privatekey = {
            rippleAddress: ""
        };
    }
    RestorekeyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RestorekeyPage');
    };
    RestorekeyPage.prototype.restoreSubmit = function () {
        // this.storage.get('address').then((XrpVal) => {
        //   if (XrpVal) {
        //     this.privatekey.rippleAddress = XrpVal;
        //   }else{
        //    // this.PrintPageWallet();
        //   }
        //   console.log("testing", this.privatekey.rippleAddress);
        // });
        this.navCtrl.push('DashboardPage', { 'key': this.privatekey.rippleAddress });
    };
    RestorekeyPage.prototype.back = function () {
        this.navCtrl.setRoot('HomePage');
    };
    RestorekeyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restorekey',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/restorekey/restorekey.html"*/'<ion-content style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n    <div class="close-btn" (click)=back()>X</div>\n    <ion-row style="margin: 50px 0 20px 0">\n        <ion-col col-1>\n        </ion-col>\n        <ion-col col-10>\n            <ion-input class="address" type="text" [(ngModel)]="privatekey.rippleAddress" placeholder="Enter Private Key"></ion-input>\n        </ion-col>\n        <ion-col col-1>\n        </ion-col>\n\n    </ion-row>\n    <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col col-8 text-center>\n            <button ion-button (click)="restoreSubmit()">Restore Wallet</button>\n        </ion-col>\n        <ion-col col-2></ion-col>\n    </ion-row>\n\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/restorekey/restorekey.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], RestorekeyPage);
    return RestorekeyPage;
}());

//# sourceMappingURL=restorekey.js.map

/***/ })

});
//# sourceMappingURL=3.js.map