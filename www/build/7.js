webpackJsonp([7],{

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EthSendPageModule", function() { return EthSendPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eth_send__ = __webpack_require__(827);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EthSendPageModule = /** @class */ (function () {
    function EthSendPageModule() {
    }
    EthSendPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eth_send__["a" /* EthSendPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eth_send__["a" /* EthSendPage */]),
            ],
        })
    ], EthSendPageModule);
    return EthSendPageModule;
}());

//# sourceMappingURL=eth-send.module.js.map

/***/ }),

/***/ 827:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EthSendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EthSendPage = /** @class */ (function () {
    function EthSendPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EthSendPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EthSendPage');
    };
    EthSendPage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    EthSendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eth-send',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/eth-send/eth-send.html"*/'\n\n<ion-content padding style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n    <div class="close-btn" (click)=back()>X</div>\n\n    <ion-row>\n        <ion-col col-4>\n          </ion-col>\n      <ion-col col-4 text-center>\n          <img class="img" width="80px" height="80px" src="./assets/imgs/etherium_logo.png">\n      </ion-col>\n      <ion-col col-4>\n\n      </ion-col>\n    </ion-row>\n    <ion-row style="margin-top: 50px">\n        \n          <ion-col col-10>  \n            <ion-input  class="address" type="text" [(ngModel)]="address" name="address" placeholder="Enter Receiving Address"></ion-input>\n          </ion-col>\n          <ion-col col-2 class="qr">\n            <button ion-button icon-only (click)="scanQrCode()">\n              <ion-icon name="qr-scanner"></ion-icon>\n              <!-- <img width="80%" src="./assets/imgs/qr.gif"> -->\n            </button>\n          </ion-col>\n          \n        </ion-row>\n\n        <ion-row>\n        \n              <ion-col col-10>  \n                <ion-input  class="address" type="number" [(ngModel)]="amount" name="amount" placeholder="Enter Amount"></ion-input>\n              </ion-col>\n             \n              <ion-col col-2></ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col col-2></ion-col>\n              <ion-col col-8>\n                  <button block ion-button class="send-btn" (click)="sendamount()">Send</button>\n                  </ion-col>\n                  </ion-row>\n                  <ion-col col-2></ion-col>\n</ion-content>\n'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/eth-send/eth-send.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], EthSendPage);
    return EthSendPage;
}());

//# sourceMappingURL=eth-send.js.map

/***/ })

});
//# sourceMappingURL=7.js.map