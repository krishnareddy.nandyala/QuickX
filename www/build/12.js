webpackJsonp([12],{

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcReceivePageModule", function() { return BtcReceivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_receive__ = __webpack_require__(535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_qrcode2__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var BtcReceivePageModule = /** @class */ (function () {
    function BtcReceivePageModule() {
    }
    BtcReceivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_receive__["a" /* BtcReceivePage */],
            ],
            imports: [__WEBPACK_IMPORTED_MODULE_3_ngx_qrcode2__["a" /* NgxQRCodeModule */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_receive__["a" /* BtcReceivePage */]),
            ],
        })
    ], BtcReceivePageModule);
    return BtcReceivePageModule;
}());

//# sourceMappingURL=btc-receive.module.js.map

/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcReceivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common_common__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BtcReceivePage = /** @class */ (function () {
    function BtcReceivePage(navCtrl, storage, socialSharing, commonService, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.socialSharing = socialSharing;
        this.commonService = commonService;
        this.navParams = navParams;
        // this.bitcoinAddress=this.commonService.walletCache.address;
        // this.BtcValue=this.storage.set('BtcAddress',this.bitcoinAddress)
        this.storage.get('BtcAddress').then(function (BtcVal) {
            if (BtcVal) {
                _this.bitcoinAddress = BtcVal.address;
            }
        });
    }
    BtcReceivePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcReceivePage');
    };
    BtcReceivePage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    BtcReceivePage.prototype.sharing = function (item) {
        var _this = this;
        this.socialSharing.share("" + this.bitcoinAddress, '', '', '')
            .then(function (data) {
            console.log(_this.bitcoinAddress);
        }).catch(function () {
        });
    };
    BtcReceivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-receive',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/btc-receive/btc-receive.html"*/'<ion-content padding style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n  <div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col col-4>\n    </ion-col>\n    <ion-col col-4 text-center>\n      <img class="img" width="80px" height="80px" src="./assets/imgs/bitcoincash_logo.png">\n    </ion-col>\n    <ion-col col-4>\n\n    </ion-col>\n  </ion-row>\n\n\n  <ion-row style="margin-top: 10px;">\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-9>\n      <div style="color: #fff; font-size: 20px; ">Address</div>\n      <textarea class="textarea" disabled>{{bitcoinAddress}}</textarea>\n    </ion-col>\n    <ion-col col-2 text-center >\n        <ion-icon style="margin-top: 30px;" name="share" (click)="sharing()"></ion-icon>\n      </ion-col>\n  </ion-row>\n  <ion-row style="margin-top: 20px">\n    <ion-col col-2>\n    </ion-col>\n    <ion-col col-8 text-center>\n      <ngx-qrcode [qrc-value]="bitcoinAddress"></ngx-qrcode>\n    </ion-col>\n    <ion-col col-2>\n      </ion-col>\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/btc-receive/btc-receive.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_2__providers_common_common__["a" /* CommonProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], BtcReceivePage);
    return BtcReceivePage;
}());

//# sourceMappingURL=btc-receive.js.map

/***/ })

});
//# sourceMappingURL=12.js.map