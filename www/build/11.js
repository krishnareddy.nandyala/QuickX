webpackJsonp([11],{

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcSendPageModule", function() { return BtcSendPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_send__ = __webpack_require__(536);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcSendPageModule = /** @class */ (function () {
    function BtcSendPageModule() {
    }
    BtcSendPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_send__["a" /* BtcSendPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_send__["a" /* BtcSendPage */]),
            ],
        })
    ], BtcSendPageModule);
    return BtcSendPageModule;
}());

//# sourceMappingURL=btc-send.module.js.map

/***/ }),

/***/ 536:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcSendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_common_common__ = __webpack_require__(265);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BitCoin = __webpack_require__(269);
/**
 * Generated class for the BtcSendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcSendPage = /** @class */ (function () {
    function BtcSendPage(barcodeScanner, alertCtrl, navCtrl, storage, commonService, navParams) {
        var _this = this;
        this.barcodeScanner = barcodeScanner;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.commonService = commonService;
        this.navParams = navParams;
        var testNet = BitCoin.networks.testnet;
        console.log(testNet);
        this.storage.get('BtcAddress').then(function (BtcVal) {
            if (BtcVal) {
                _this.bitcoinAddress = BtcVal.address;
                _this.getBTCBalance();
            }
        });
        this.portData = this.navParams.get('prot');
        console.log("pportdata:", this.portData);
    }
    BtcSendPage.prototype.ionViewDidLoad = function () {
    };
    BtcSendPage.prototype.scanQrCode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            if (barcodeData && barcodeData.text) {
                _this.toAddress = barcodeData.text;
                console.log(_this.toAddress);
            }
        }, function (err) {
            //  this.logger.error(err);
        });
    };
    BtcSendPage.prototype.send = function () {
        var testNet = BitCoin.networks.testnet;
        console.log(testNet);
        var tx = new BitCoin.TransactionBuilder(testNet);
        var url = 'http://api.blockcypher.com/v1/btc/test3';
        // this.commonService.getApi(url + this.bitcoinAddress + '/utxo')
        //   .subscribe(data => {
        //     console.log("BTC simha:", data)
        //   },
        //     err => {
        //       console.log('error')
        //     })
    };
    BtcSendPage.prototype.getBTCBalance = function () {
        this.commonService.getApi('https://api.blockcypher.com/v1/btc/test3/addrs/' + this.bitcoinAddress)
            .subscribe(function (data) {
            console.log("BTC Narasimha:", data);
        }, function (err) {
            console.log('error');
        });
    };
    BtcSendPage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    BtcSendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-send',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/btc-send/btc-send.html"*/'<!-- <ion-header>\n\n  <ion-navbar color="dark">\n   \n    <ion-title>Send Funds</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="scanQrCode()">\n        <ion-icon name="qr-scanner"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n    <div class="close-btn" (click)=back()>X</div>\n\n    <ion-row>\n        <ion-col col-4>\n          </ion-col>\n      <ion-col col-4 text-center>\n          <img class="img" width="80px" height="80px" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n      <ion-col col-4>\n\n      </ion-col>\n    </ion-row>\n    <ion-row style="margin-top: 50px">\n        \n          <ion-col col-10>  \n            <ion-input  class="address" type="text" \n            [(ngModel)]="toAddress" name="toAddress"\n             placeholder="Enter Receiving Address"></ion-input>\n          </ion-col>\n          <ion-col col-2 class="qr">\n            <button ion-button icon-only (click)="scanQrCode()">\n              <ion-icon name="qr-scanner"></ion-icon>\n              <!-- <img width="80%" src="./assets/imgs/qr.gif"> -->\n            </button>\n          </ion-col>\n\n          \n          \n        </ion-row>\n\n        <ion-row>\n           \n              <ion-col col-10>  \n                <ion-input  class="address" type="number" [(ngModel)]="toAmount"\n                 name="toAmount" placeholder="Enter Amount"></ion-input>\n              </ion-col>\n             \n              <ion-col col-2></ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col col-2></ion-col>\n              <ion-col col-8>\n                  <button block ion-button class="send-btn" (click)="prepareSend()">Send</button>\n                  </ion-col>\n                  </ion-row>\n                  <ion-col col-2></ion-col>\n</ion-content>\n\n<!-- <ion-header>\n\n  <ion-navbar color="dark">\n    <ion-title>Send Funds</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="scanQrCode()">\n        <ion-icon name="qr-scanner"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <form (ngSubmit)="prepareSend()">\n    <ion-list>\n      <ion-item>\n        <ion-label stacked>Bitcoin address </ion-label>\n        <ion-input type="text" class="btc" [(ngModel)]="toAddress" name="toAddress"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="toAmount" name="toAmount"></ion-input>\n      </ion-item>\n\n    </ion-list>\n    <button type="submit" [disabled]="!toAddress || !toAmount" ion-button block>send</button>\n  </form>\n\n</ion-content> -->\n'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/btc-send/btc-send.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_common_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], BtcSendPage);
    return BtcSendPage;
}());

//# sourceMappingURL=btc-send.js.map

/***/ })

});
//# sourceMappingURL=11.js.map