import { Component } from '@angular/core';
import * as ripple from 'ripple-lib';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonProvider } from '../../providers/common/common';

@Component({
  selector: 'xrp',
  templateUrl: 'xrp.html'
})
export class XrpComponent {
  XRPStorage: { address: any; privateKey: any; };
  rippleAddress: any;
  ripplePrivateKey: any;
  storageAdd;
  setValue;

  constructor( public commonService : CommonProvider, private storage: Storage,public navCtrl: NavController) {
   // this.storage.remove('Xrpaddress');
    this.storage.get('Xrpaddress').then((XrpVal) => {
      if (XrpVal) {
        this.rippleAddress = XrpVal.address;
      }else{
        this.PrintPageWallet();
      }
    //  console.log("testing", this.storageAdd);
    });
  
  }
  PrintPageWallet() {
    var api = new ripple.RippleAPI();
    var account = api.generateAddress();
    this.rippleAddress = account["address"];
    this.ripplePrivateKey = account["secret"];


    this.XRPStorage = {
      address: this.rippleAddress,
      privateKey: this.ripplePrivateKey
    }
    this.storage.set('Xrpaddress', this.XRPStorage);
    

    var storageKey = this.storage.get('key');
    //console.log(api);
    console.log(this.setValue);
    // console.log(storageKey);

  }
  send(){
    this.navCtrl.push('XrpSendPage')
  }
  amountReceive(){
    this.navCtrl.setRoot('XrpReceivePage')
  }
  portfolio(){
    this.navCtrl.setRoot('PortfolioPage');
  }
}
