import { NgModule } from '@angular/core';
import { EthComponent } from './eth/eth';
import { BtcComponent } from './btc/btc';
import { XrpComponent } from './xrp/xrp';
@NgModule({
	declarations: [EthComponent,
    BtcComponent,
    XrpComponent,
],
	imports: [],
	exports: [EthComponent,
    BtcComponent,
    XrpComponent,]
})
export class ComponentsModule {}
