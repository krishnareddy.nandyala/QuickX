import { Component, Input, OnInit } from '@angular/core';
import { CommonProvider } from '../../providers/common/common';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
/**
 * Generated class for the BtcComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'btc',
  templateUrl: 'btc.html'
})


export class BtcComponent implements OnInit {
  getBalance: Object;
  BtcValue: any;
  bitcoinAddress: any;
  btcPrivateKey: any;
  qrcode: any;
  walletName: any;
  errorMsg: any;
  
  @Input()
  portFolio: any;

  constructor(public commonService: CommonProvider, private storage: Storage,
    public navCtrl: NavController, private toastCtrl: ToastController ) {
    //this.storage.remove('Ethaddress');

    this.storage.get('BtcAddress').then((BtcVal) => {
      if (BtcVal) {
        this.bitcoinAddress = BtcVal.address;
      } else {
        this.commonService.create('kiran');
        this.generateBtcAddress();
      }
    });
  }
  BTCStorage:any
  bitcoinPrivatekey:any

  ngOnInit(){
    this.storage.get('BtcAddress').then((BtcVal) => {
      if (BtcVal) {
        this.bitcoinAddress = BtcVal.address;
      } else {
        this.commonService.create('kiran');
        this.generateBtcAddress();
      }
    });

  }
  // Get BTC Address
  generateBtcAddress() {
    this.bitcoinAddress = this.commonService.walletCache.address;
    this.bitcoinPrivatekey=this.commonService.walletCache.wif;

    this.BTCStorage = {
      address: this.bitcoinAddress,
      privateKey: this.bitcoinPrivatekey
    }
    this.storage.set('BtcAddress', this.BTCStorage);
   
  }
  amountSend() {
    this.navCtrl.setRoot('BtcSendPage',{prot:"portFolio"});
   
  }
  amountReceive() {
    this.navCtrl.setRoot('BtcReceivePage');
  }
  portfolio() {
    this.navCtrl.setRoot('PortfolioPage');
  }
}