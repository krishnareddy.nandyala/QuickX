import { Component } from '@angular/core';
import ethers from 'ethers';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonProvider } from '../../providers/common/common';

@Component({
  selector: 'eth',
  templateUrl: 'eth.html'
})
export class EthComponent {
  ETHStorage: {};
  ethPrivateKey: any;
  newWallet = {
    password: '',
    privateKey: '',
    address: '',
    keyObject: '',
    keyFile: '',
    fileName: '',
    unEncodedFile: ''
  };
  walletKeys: any;
  text: string;

  constructor(public commonService: CommonProvider, private storage: Storage, public navCtrl: NavController) {
    //this.storage.remove('Ethaddress');
    this.storage.get('Ethaddress').then((EthVal) => {
      if (EthVal) {
        this.newWallet.address = EthVal.address;
      } else {
        this.createRandomWallet();
      }
    });
  }
  createRandomWallet() {
    let walletKeys = ethers.Wallet.createRandom(this.newWallet);
    this.newWallet.address = walletKeys.address;
    this.newWallet.privateKey = walletKeys.privateKey;
    this.ETHStorage = {
      address: this.newWallet.address,
      privateKey: this.newWallet.privateKey
    }
    this.storage.set('Ethaddress', this.ETHStorage);
    
  }
  send() {
    this.navCtrl.setRoot('EthSendPage')
  }
  amountReceive() {
    this.navCtrl.setRoot('EthReceivePage')
  }
  portfolio() {
    this.navCtrl.setRoot('PortfolioPage');
  }
}
