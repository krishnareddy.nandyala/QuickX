import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as BitcoinLib from 'bitcoinjs-lib';
import * as Bip39 from 'bip39';
//import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/*
  Generated class for the CommonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommonProvider {
  address: any;
  wallet: any;


  network: any;
  walletCache: any;
  constructor(public http: HttpClient) {
    this.network = BitcoinLib.networks.testnet;
    this.create("kiran").then((x) => {
    },
      (err) => {
        console.log(err);
      })
  }
  create(name?: string) {
    return new Promise((resolve, reject) => {
      try {
        let keyPair = BitcoinLib.ECPair.makeRandom({ network: this.network });
        this.walletCache = {
          wif: keyPair.toWIF(),
          address: keyPair.getAddress(),
          name: name ? name : 'My bitcoin wallet'
        };

        resolve(this.walletCache);
      } catch (err) {

        reject(err);
      };
    });
  }

  // get slider data

  getSliderData(url) {
    return this.http.get(url)
      .catch(this.errorHandler);
  }

  getBalance(url) {
    return this.http.get(url)
      .catch(this.errorHandler);
  }
  getApi(url) {
    return this.http.get(url)
      .catch(this.errorHandler);
  }

  // errorHandler
  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }

}
