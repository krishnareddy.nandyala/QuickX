import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { CommonProvider } from '../providers/common/common';
import { SocialSharing } from '@ionic-native/social-sharing';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@NgModule({
  declarations: [
    MyApp,
    ListPage,
   // EthComponent
  ],
  imports: [
    BrowserModule,IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule,
    HttpClientModule,

   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    //EthComponent,
    ListPage
  ],
  providers: [
    StatusBar,
    SocialSharing,
    //Storage,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonProvider,
    BarcodeScanner,
    
  ]
})
export class AppModule {}
