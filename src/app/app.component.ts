import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  pwds: any;
  @ViewChild(Nav) nav: Nav;

  rootPage: string = "PasswordPage";

  pages: Array<{title: string, component: string, src:any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, 
    public splashScreen: SplashScreen,public storage:Storage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      
      //{ title: 'List', component: ListPage },
      { title: 'Restore', component: "RestorekeyPage", src:'../assets/imgs/restore.png'},
      { title: 'Log Out', component: "PasswordPage", src:'../assets/imgs/logout.png'}
    ];
    // this.storage.remove('passwords');
  // this.storage.get('passwords').then(x=>{
  //   this.pwds = x;
  //   console.log("krishna",this.pwds);
  //   if(this.pwds){
  //    this.nav.push('HomePage');
  //   }
  //   else{
  //     this.nav.push('PasswordPage');
  //   }
  // })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(pages){
   
    this.nav.setRoot(pages.component);
  }
}
