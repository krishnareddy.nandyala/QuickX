import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-portfolio',
  templateUrl: 'portfolio.html',
})
export class PortfolioPage {

  XrpAddress: any;
  EthAddress: any;
  BtcAddress: any;
  date: Date;
  total: any;
  btcbal: any;
  ethBal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.storage.get('btcCurrentBalancec').then(x => {
      this.storage.get('ethCurrentBalancec').then(y => {
        this.btcbal = x;
        this.ethBal = y;
        this.total = this.btcbal + this.ethBal;
        this.date = new Date();
      })
    })

    console.log("this.total", this.total)
  }

  ionViewDidLoad() {
    this.allAddress();
  }
  allAddress() {
    this.storage.get('BtcAddress')
      .then((x) => {
        this.BtcAddress = x.address;
        
      })
    this.storage.get('Ethaddress').then((y) => {
      this.EthAddress = y.address;
    });
    this.storage.get('Xrpaddress').then((z) => {
      this.XrpAddress = z.address;

    });
  }
  back() {
    this.navCtrl.setRoot('DashboardPage')
  }


}
