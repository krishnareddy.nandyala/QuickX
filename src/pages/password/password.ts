import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {

  pwds: any;
  numbers = [1, 2, 3];d
  numbers1 = [4, 5, 6];
  numbers2 = [7, 8, 9];
  numbers3 = ['', 0, '']

  password = [];

  lines = ['_', '_', '_', '_', '_', '_'];

  // bcolor: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordPage');
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Password Incorrect',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  // backgroudColor;

  number(num) {

    if (this.password.length <= 5) {
      this.password.push(num);
    }
    console.log("add", this.password)
    if (this.password.length == 6) {
       // this.storage.remove('passwords');
      this.storage.get('passwords').then(x => {
        this.pwds = x;
        console.log("krishna", this.pwds);

        if (this.pwds) {
          if (Array.prototype.toString.call(this.password) == Array.prototype.toString.call(this.pwds)) {
            this.navCtrl.setRoot('HomePage');
          }
          else {
            this.presentToast();
          }
        }
        else {
          this.navCtrl.setRoot("ConformpasswordPage", { 'password': this.password })
        }
      })

    }
  }

  cancel() {
    this.password.splice(this.password.length - 1, 1);
    console.log("cancel", this.password);

  }

}
