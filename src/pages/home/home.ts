import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  bitcoinAddress: any;
  createbutton :boolean = false;
  continue : boolean = true;

  constructor(public navCtrl: NavController, private storage: Storage, public navParams: NavParams) {

    this.storage.get('BtcAddress').then((BtcVal) => {
      if (BtcVal) {
        this.bitcoinAddress = BtcVal;
        this.createbutton = true;
        this.continue = false; 
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  create(){
    this.navCtrl.setRoot('DashboardPage')
  }
  Continue(){
    this.navCtrl.push('DashboardPage');
  }
  restore() {
    this.navCtrl.setRoot('RestorekeyPage');
  }
}
