import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CommonProvider} from '../../providers/common/common';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
@IonicPage()
@Component({
  selector: 'page-btc-receive',
  templateUrl: 'btc-receive.html',
})
export class BtcReceivePage {
  BtcValue: any;
  bitcoinAddress: any;
  btcPrivateKey: any;
  qrcode: any;
  walletName: any;
  constructor(public navCtrl: NavController,private storage: Storage,
    public socialSharing: SocialSharing,
    public commonService : CommonProvider, public navParams: NavParams) {
      // this.bitcoinAddress=this.commonService.walletCache.address;
      // this.BtcValue=this.storage.set('BtcAddress',this.bitcoinAddress)
      this.storage.get('BtcAddress').then((BtcVal) => {
        if (BtcVal) {
          this.bitcoinAddress = BtcVal.address;
        }
      });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcReceivePage');
  }
  back(){
    this.navCtrl.setRoot('DashboardPage')
  }
 
  sharing(item) {
    this.socialSharing.share(`${this.bitcoinAddress}`, '','', '')
      .then(data => {
        console.log(this.bitcoinAddress);
      }).catch(() => {

      })
  }

  }





 


