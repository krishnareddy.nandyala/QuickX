import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcReceivePage } from './btc-receive';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    BtcReceivePage,
  ],
  imports: [ NgxQRCodeModule,
    IonicStorageModule.forRoot(),
   
    IonicPageModule.forChild(BtcReceivePage),
  ],
})
export class BtcReceivePageModule {}
