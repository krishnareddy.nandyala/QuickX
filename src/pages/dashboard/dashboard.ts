import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { CommonProvider } from '../../providers/common/common';
import { BtcComponent } from '../../components/btc/btc'
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  currentbal: {};
  XrpAddress: any;
  XRPBalance: any;
  XRPcurrentBalance: number;
  sliderXrpValue: any;
  BTCcurrentBalance: number;
  ETHcurrentBalance: number;
  portfolio: {};
  test: any;
  BTCBalance: any;
  ETHBalance: any;
  
  xrp: {};
  slide: {};
  Eth: boolean = false;
  Btc: boolean = false;
  Xrp: boolean = false;
  sliderRippleValue: any;
  sliderEthereumValue: any;
  sliderBitcoinValue: any;
  slideinfo = [1, 2, 3];

  @ViewChild(Slides) slides: Slides;
  
  BtcAddress;
  EthAddress;

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    public navParams: NavParams,
    public commonService: CommonProvider,
    public storage: Storage) {
      this.selectBtc();

    /* BTC(Bitcoin) start */
    this.storage.get('BtcAddress')
      .then((x) => {
        if(x){
        this.BtcAddress = x.address;
        this.getBtcBalance();
        }else{
          this.BTCBalance=0;
          this.BTCcurrentBalance=0
        }
      })
    /* BTC(Bitcoin) end */

    /* ETH(Ethereum) start */

    this.storage.get('Ethaddress').then((y) => {
      if(y){
      this.EthAddress = y.address;
      this.getEthBalance();
      }else{
        this.ETHBalance=0;
        this.ETHcurrentBalance=0;
      }
    });
    /* ETH(Ethereum) end */


    /* XRP(Ripple) start */
    this.storage.get('Xrpaddress').then((z) => {
      if(z){
      this.XrpAddress = z.address;
      // this.getXrpBalance();
      }
      else{
      
      }
    });
    /* XRP(Ripple) end */

    /* restore key start */
    // var restoreKey = this.navParams.get('key');

    // this.storage.get('address').then((XrpVal) => {
    //   this.storage.get('Ethaddress').then((EthVal) => {
    //     this.storage.get('BtcAddress').then((BtcVal) => {
    //       if (restoreKey == XrpVal) {
    //         this.selectXrp();
    //       }
    //       else if (restoreKey == EthVal) {
    //         this.selectEth();
    //       }
    //       else if (restoreKey == BtcVal) {
    //         this.selectBtc();
    //       }
    //     })
    //   })
    // })
    /* restore key end */

  }


  /* toster start */
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Error',

      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  /* toster end */



  BTC() {
    var url = "https://api.coinmarketcap.com/v2/ticker/1/?convert=INR";
    this.commonService.getSliderData(url)
      .subscribe((data: any) => {
        this.sliderBitcoinValue = data.data.quotes.INR.price;
        this.BTCcurrentBalance = (data.data.quotes.INR.price * this.BTCBalance) / 100000000;
        this.storage.set('btcCurrentBalancec', this.BTCcurrentBalance);
     
      })
  }

  ETH() {
    var url = "https://api.coinmarketcap.com/v2/ticker/1027/?convert=INR";
    this.commonService.getSliderData(url)
      .subscribe((data: any) => {
        this.sliderEthereumValue = data.data.quotes.INR.price;
        // this.getEthBalance = data.result,
        this.ETHcurrentBalance = (data.data.quotes.INR.price * this.ETHBalance) / 100000000;
        this.storage.set('ethCurrentBalancec', this.ETHcurrentBalance);
      })
  }

  XRP() {
    var url = "https://api.coinmarketcap.com/v2/ticker/52/?convert=INR";
    this.commonService.getSliderData(url)
      .subscribe((data: any) => {
        this.sliderRippleValue = data.data.quotes.INR.price;
        this.XRPcurrentBalance = (data.data.quotes.INR.price * this.ETHBalance) / 100000000;
      })
  }


  /* BTC getBalance start */
  getBtcBalance() {
    var url = "http://api.blockcypher.com/v1/btc/test3/addrs/"+this.BtcAddress;
    this.commonService.getBalance(url)
      .subscribe(data => {
        console.log("getBalance:", data);
        this.BTCBalance = data['balance'],
          error => this.presentToast();
        this.BTC();
       
      })
  }
  /* BTC getBalance end */

  /* ETH getBalance start */
  getEthBalance() {
    var url = "https://api-rinkeby.etherscan.io/api?module=account&action=balance&address="+this.EthAddress;
    this.commonService.getBalance(url)
      .subscribe(data => {
        console.log("getEthBalance:", data);
        this.ETHBalance = data.result,
          error => this.presentToast();
        this.ETH();
       
      })
  }
  /* ETH getBalance start */

  /* XRP getBalance start */
  getXrpBalance() {
    var url = "https://data.ripple.com/v2/accounts/"+this.XrpAddress;
    this.commonService.getBalance(url)
      .subscribe(data => {
        this.XRPBalance = data.value,
          error => this.presentToast();
        //  this.XRP();
      })
  }
  /* XRP getBalance end */

  ionViewDidLoad() {
  }

  /* slide change start */
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    if (currentIndex == 0) {
      this.selectBtc();
    }
    if (currentIndex == 1) {
      this.selectXrp();
    }
    if (currentIndex == 2) {
      this.selectEth();
    }
  }
  /* slide change end */

  selectBtc() {
    this.Btc = true;
    this.Eth = false;
    this.Xrp = false;
  }
  selectXrp() {
    this.Btc = false;
    this.Eth = false;
    this.Xrp = true;
  }
  selectEth() {
    this.Btc = false;
    this.Eth = true;
    this.Xrp = false;
  }
  // ionslides(val) {
  //   alert(val);
  // }

}
