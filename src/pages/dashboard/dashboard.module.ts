import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import {EthComponent} from '../../components/eth/eth';
import {BtcComponent} from '../../components/btc/btc';
import {XrpComponent} from '../../components/xrp/xrp';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { IonicStorageModule } from '@ionic/storage';
import {CommonProvider} from '../../providers/common/common';
@NgModule({
  declarations: [
    DashboardPage,
    EthComponent,
    BtcComponent,
    XrpComponent
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    NgxQRCodeModule,
    IonicStorageModule.forRoot(),
  ],
  providers:[
    CommonProvider,
  ]
})
export class DashboardPageModule {}
