import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { XrpReceivePage } from './xrp-receive';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { IonicStorageModule } from '@ionic/storage';
@NgModule({
  declarations: [
    XrpReceivePage,
  ],
  imports: [
    NgxQRCodeModule,
    IonicStorageModule.forRoot(),
    IonicPageModule.forChild(XrpReceivePage),
  ],
})
export class XrpReceivePageModule {}
