import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as ripple from 'ripple-lib';
import { Storage } from '@ionic/storage';
import { CommonProvider } from '../../providers/common/common';
import { SocialSharing } from '@ionic-native/social-sharing';
@IonicPage()
@Component({
  selector: 'page-xrp-receive',
  templateUrl: 'xrp-receive.html',
})
export class XrpReceivePage {
  rippleAddress: any;
  ripplePrivateKey: any;
  storageAdd;
  setValue;


  constructor(public commonService: CommonProvider, public storage: Storage,
    public navCtrl: NavController,
    public socialSharing: SocialSharing,
    public navParams: NavParams) {
      this.storage.get('Xrpaddress').then((XrpVal) => {
        if (XrpVal) {
          this.rippleAddress = XrpVal.address;
        }
      //  console.log("testing", this.storageAdd);
      });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad XrpReceivePage');
  }
  back() {
    this.navCtrl.setRoot('DashboardPage')
  }


  sharing(item) {
    this.socialSharing.share(`${this.rippleAddress}`, '','', '')
      .then(data => {
        console.log(this.rippleAddress);
      }).catch(() => {

      })
  }
}
