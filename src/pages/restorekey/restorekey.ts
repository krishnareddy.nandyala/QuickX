import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the RestorekeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restorekey',
  templateUrl: 'restorekey.html',
})
export class RestorekeyPage {
  rippleAddress
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {

  }
  privatekey={
    rippleAddress:""
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RestorekeyPage');
  }

  restoreSubmit(){
    // this.storage.get('address').then((XrpVal) => {
    //   if (XrpVal) {
    //     this.privatekey.rippleAddress = XrpVal;
    //   }else{
    //    // this.PrintPageWallet();
    //   }
    //   console.log("testing", this.privatekey.rippleAddress);
    // });
this.navCtrl.push('DashboardPage',{'key':this.privatekey.rippleAddress});
  }
  back() {
    this.navCtrl.setRoot('HomePage')
  }

}
