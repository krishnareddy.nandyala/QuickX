import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestorekeyPage } from './restorekey';

@NgModule({
  declarations: [
    RestorekeyPage,
  ],
  imports: [
    IonicPageModule.forChild(RestorekeyPage),
  ],
})
export class RestorekeyPageModule {}
