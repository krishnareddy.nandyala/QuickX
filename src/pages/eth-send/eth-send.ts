import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-eth-send',
  templateUrl: 'eth-send.html',
})
export class EthSendPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthSendPage');
  }
  back(){
    this.navCtrl.setRoot('DashboardPage')
  }
}
