import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthSendPage } from './eth-send';

@NgModule({
  declarations: [
    EthSendPage,
  ],
  imports: [
    IonicPageModule.forChild(EthSendPage),
  ],
})
export class EthSendPageModule {}
