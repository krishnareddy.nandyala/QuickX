import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthDashboardPage } from './eth-dashboard';

@NgModule({
  declarations: [
    EthDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(EthDashboardPage),
  ],
})
export class EthDashboardPageModule {}
