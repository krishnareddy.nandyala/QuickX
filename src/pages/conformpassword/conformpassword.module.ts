import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConformpasswordPage } from './conformpassword';

@NgModule({
  declarations: [
    ConformpasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ConformpasswordPage),
  ],
 
})
export class ConformpasswordPageModule {}
