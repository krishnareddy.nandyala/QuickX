import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-conformpassword',
  templateUrl: 'conformpassword.html',
})
export class ConformpasswordPage {

  pwds: any;
  pwd: Promise<any>;
  password = [];
  numbers = [1, 2, 3];
  numbers1 = [4, 5, 6];
  numbers2 = [7, 8, 9];
  numbers3 = ['', 0, '']

  empty = ['', '', '', '', '', ''];
  password2 = [];
  lines = ['_', '_', '_', '_', '_', '_'];


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toastCtrl: ToastController,private storage: Storage) {
    this.password = this.navParams.get("password")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConformpasswordPage');
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Password Mismatch',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  number(num) {

    if (this.password2.length <= 5) {
      this.password2.push(num);
    }
    console.log("add", this.password2)

    if (this.password2.length == 6) {
      if (Array.prototype.toString.call(this.password) == Array.prototype.toString.call(this.password2)) {
        
        this.storage.set('passwords', this.password);
     
        this.navCtrl.setRoot("HomePage")
      }
      else {
        this.presentToast();
      }
    }
  }
  cancel() {
    this.password2.splice(this.password2.length - 1, 1);
    console.log("cancel", this.password2);

  }
  back(){
    this.navCtrl.setRoot('PasswordPage')
  }
}
