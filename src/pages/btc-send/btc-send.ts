import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, Platform, LoadingController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { Storage } from '@ionic/storage';
import { CommonProvider } from '../../providers/common/common';
var BitCoin = require('bitcoinjs-lib');
/**
 * Generated class for the BtcSendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-send',
  templateUrl: 'btc-send.html',
})
export class BtcSendPage {
  bitcoinAddress: any;
  portData: any;
  private toAddress: string;
  private toAmount: number;

  constructor(private barcodeScanner: BarcodeScanner,

    public alertCtrl: AlertController,
    public navCtrl: NavController,
    private storage: Storage,
    private commonService: CommonProvider,
    public navParams: NavParams) {
      let testNet =  BitCoin.networks.testnet;
      console.log(testNet);
    this.storage.get('BtcAddress').then((BtcVal) => {
      if (BtcVal) {
        this.bitcoinAddress = BtcVal.address;
        this.getBTCBalance();
      }
    });
    this.portData = this.navParams.get('prot');
    console.log("pportdata:", this.portData);
  }


  ionViewDidLoad() {
  }

  scanQrCode() {
    this.barcodeScanner.scan().then((barcodeData) => {
      if (barcodeData && barcodeData.text) {
        this.toAddress = barcodeData.text;
        console.log(this.toAddress)
      }
    }, (err) => {
      //  this.logger.error(err);
    });
  }



  send() {
    let testNet =  BitCoin.networks.testnet;
      console.log(testNet);
      let tx = new BitCoin.TransactionBuilder(testNet)
     
    var url = 'http://api.blockcypher.com/v1/btc/test3';
   
  
    // this.commonService.getApi(url + this.bitcoinAddress + '/utxo')
    //   .subscribe(data => {
    //     console.log("BTC simha:", data)
    //   },
    //     err => {
    //       console.log('error')
    //     })
  }

  getBTCBalance() {

    this.commonService.getApi('https://api.blockcypher.com/v1/btc/test3/addrs/' + this.bitcoinAddress)
      .subscribe(data => {
        console.log("BTC Narasimha:", data)
      },
        err => {
          console.log('error')
        })
  }


  back() {
    this.navCtrl.setRoot('DashboardPage')
  }
}
