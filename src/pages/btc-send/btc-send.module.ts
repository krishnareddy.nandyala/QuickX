import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcSendPage } from './btc-send';

@NgModule({
  declarations: [
    BtcSendPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcSendPage),
  ],
})
export class BtcSendPageModule {}
