import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CommonProvider } from '../../providers/common/common';
import ethers from 'ethers';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-eth-receive',
  templateUrl: 'eth-receive.html',
})
export class EthReceivePage {
  setValue: any;
  ethPrivateKey: any;
  newWallet = {
    password: '',
    privateKey: '',
    address:'',
    keyObject:'',
    keyFile: '',
    fileName: '',
    unEncodedFile: ''

  };
  walletKeys:any;
  text: string;


  constructor(public commonService : CommonProvider, public storage: Storage, 
    public socialSharing: SocialSharing,
    public navCtrl: NavController, public navParams: NavParams) {
    // let walletKeys = ethers.Wallet.createRandom(this.newWallet);
    // this.newWallet.address = this.walletKeys.address;
    //  this.newWallet.privateKey = this.walletKeys.privateKey;
    this.storage.get('Ethaddress').then((EthVal) => {
      if (EthVal) {
        this.newWallet.address = EthVal.address;
      } 
    });
   
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthReceivePage');
  }
  back(){
    this.navCtrl.setRoot('DashboardPage')
  }
  sharing(item) {
    this.socialSharing.share(`${this.newWallet.address}`, '','', '')
      .then(data => {
        console.log(this.newWallet.address);
      }).catch(() => {

      })
  }
}
