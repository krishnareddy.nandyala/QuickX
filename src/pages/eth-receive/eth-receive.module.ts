import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthReceivePage } from './eth-receive';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    EthReceivePage,
  ],
  imports: [
    NgxQRCodeModule,
    IonicStorageModule.forRoot(),
    IonicPageModule.forChild(EthReceivePage),
  ],
})
export class EthReceivePageModule {}
