import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-xrp-send',
  templateUrl: 'xrp-send.html',
})
export class XrpSendPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad XrpSendPage');
  }
  back(){
    this.navCtrl.setRoot('DashboardPage')
  }
}
