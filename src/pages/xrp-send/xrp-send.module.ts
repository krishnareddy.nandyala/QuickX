import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { XrpSendPage } from './xrp-send';

@NgModule({
  declarations: [
    XrpSendPage,
  ],
  imports: [
    IonicPageModule.forChild(XrpSendPage),
  ],
})
export class XrpSendPageModule {}
