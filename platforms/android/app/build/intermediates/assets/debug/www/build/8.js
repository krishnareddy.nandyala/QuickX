webpackJsonp([8],{

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EthReceivePageModule", function() { return EthReceivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eth_receive__ = __webpack_require__(826);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_qrcode2__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var EthReceivePageModule = /** @class */ (function () {
    function EthReceivePageModule() {
    }
    EthReceivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eth_receive__["a" /* EthReceivePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ngx_qrcode2__["a" /* NgxQRCodeModule */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eth_receive__["a" /* EthReceivePage */]),
            ],
        })
    ], EthReceivePageModule);
    return EthReceivePageModule;
}());

//# sourceMappingURL=eth-receive.module.js.map

/***/ }),

/***/ 826:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EthReceivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_common_common__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EthReceivePage = /** @class */ (function () {
    function EthReceivePage(commonService, storage, socialSharing, navCtrl, navParams) {
        var _this = this;
        this.commonService = commonService;
        this.storage = storage;
        this.socialSharing = socialSharing;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.newWallet = {
            password: '',
            privateKey: '',
            address: '',
            keyObject: '',
            keyFile: '',
            fileName: '',
            unEncodedFile: ''
        };
        // let walletKeys = ethers.Wallet.createRandom(this.newWallet);
        // this.newWallet.address = this.walletKeys.address;
        //  this.newWallet.privateKey = this.walletKeys.privateKey;
        this.storage.get('Ethaddress').then(function (EthVal) {
            if (EthVal) {
                _this.newWallet.address = EthVal.address;
            }
        });
    }
    EthReceivePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EthReceivePage');
    };
    EthReceivePage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    EthReceivePage.prototype.sharing = function (item) {
        var _this = this;
        this.socialSharing.share("" + this.newWallet.address, '', '', '')
            .then(function (data) {
            console.log(_this.newWallet.address);
        }).catch(function () {
        });
    };
    EthReceivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eth-receive',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/eth-receive/eth-receive.html"*/'<ion-content padding style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n  <div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col col-4>\n    </ion-col>\n    <ion-col col-4>\n      <img class="img" width="80px" height="80px" src="./assets/imgs/etherium_logo.png">\n    </ion-col>\n    <ion-col col-4>\n\n    </ion-col>\n  </ion-row>\n\n\n  <ion-row style="margin-top: 10px;">\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-9>\n      <span style="color: #fff; font-size: 20px; ">Address</span>\n      <textarea class="textarea" disabled>{{newWallet.address}}</textarea>\n\n      \n    </ion-col>\n    <ion-col col-2>\n        <ion-icon style="margin-top: 30px;" name="share" (click)="sharing()"></ion-icon>\n      </ion-col>\n  </ion-row>\n  <ion-row style="margin-top: 20px">\n    <ion-col col-2>\n    </ion-col>\n    <ion-col col-8 text-center>\n      <ngx-qrcode [qrc-value]="newWallet.address"></ngx-qrcode>\n    </ion-col>\n    <ion-col col-2>\n      </ion-col>\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/eth-receive/eth-receive.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_common_common__["a" /* CommonProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], EthReceivePage);
    return EthReceivePage;
}());

//# sourceMappingURL=eth-receive.js.map

/***/ })

});
//# sourceMappingURL=8.js.map