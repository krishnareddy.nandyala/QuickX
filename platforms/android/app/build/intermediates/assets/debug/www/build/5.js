webpackJsonp([5],{

/***/ 434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordPageModule", function() { return PasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__password__ = __webpack_require__(829);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PasswordPageModule = /** @class */ (function () {
    function PasswordPageModule() {
    }
    PasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__password__["a" /* PasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__password__["a" /* PasswordPage */]),
            ],
        })
    ], PasswordPageModule);
    return PasswordPageModule;
}());

//# sourceMappingURL=password.module.js.map

/***/ }),

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PasswordPage = /** @class */ (function () {
    // bcolor: any;
    function PasswordPage(navCtrl, navParams, storage, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.numbers = [1, 2, 3];
        this.numbers1 = [4, 5, 6];
        this.numbers2 = [7, 8, 9];
        this.numbers3 = ['', 0, ''];
        this.password = [];
        this.lines = ['_', '_', '_', '_', '_', '_'];
    }
    PasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PasswordPage');
    };
    PasswordPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Password Incorrect',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // backgroudColor;
    PasswordPage.prototype.number = function (num) {
        var _this = this;
        if (this.password.length <= 5) {
            this.password.push(num);
        }
        console.log("add", this.password);
        if (this.password.length == 6) {
            // this.storage.remove('passwords');
            this.storage.get('passwords').then(function (x) {
                _this.pwds = x;
                console.log("krishna", _this.pwds);
                if (_this.pwds) {
                    if (Array.prototype.toString.call(_this.password) == Array.prototype.toString.call(_this.pwds)) {
                        _this.navCtrl.setRoot('HomePage');
                    }
                    else {
                        _this.presentToast();
                    }
                }
                else {
                    _this.navCtrl.setRoot("ConformpasswordPage", { 'password': _this.password });
                }
            });
        }
    };
    PasswordPage.prototype.cancel = function () {
        this.password.splice(this.password.length - 1, 1);
        console.log("cancel", this.password);
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/password/password.html"*/'<ion-content  style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n  <!-- <div class="close-btn" (click)=back()>X</div> -->\n\n\n\n\n  <ion-row>\n    \n    <ion-col col-12 text-center>\n      <img class="img" src="./assets/imgs/quickx.png">\n    </ion-col>\n    \n  </ion-row>\n  <ion-row>\n    <ion-col col-12 text-center style="color: #fff; margin-top:5%;">\n      <h4 style="font-family: Poppins">Enter Application Pin</h4>\n    </ion-col>\n  </ion-row>\n  <div>\n  <ion-row  style="height:38px;">\n\n    <div *ngFor="let item of password" >\n      <ion-col col-2 text-center style="padding:0px 5px; ">\n        <div class="fill" [ngStyle]="{\'background-color\':item ? \'white\' : \'\'}"></div>\n      </ion-col>\n    </div>\n  \n  </ion-row>\n</div>\n  <ion-row >\n   \n      \n    <div *ngFor="let item of lines" style="text-align: center; ">\n      <ion-col col-2 >\n        <div  style="color:#fff; width: 17px; height: 17px;margin: 0 8px; text-align: center">{{item}}</div>\n      </ion-col>\n    </div>\n  \n  </ion-row>\n  <ion-row>\n    <ion-col col-12 text-center style="color: lightgray; padding:0px;">     \n    </ion-col>\n    <ion-col col-3>\n    </ion-col>\n  </ion-row>\n\n</ion-content>\n<ion-footer>\n  <div *ngFor="let item of numbers">\n    <div col-4 class="num-keypad" (click)="number(item)" style="float: left;">\n      <span>{{item}}</span>\n    </div>\n  </div>\n  <div *ngFor="let item of numbers1">\n    <div col-4 class="num-keypad" (click)="number(item)" style="float:left;">\n      <span>{{item}}</span>\n    </div>\n  </div>\n  <div *ngFor="let item of numbers2">\n    <div col-4 class="num-keypad" (click)="number(item)" style="float:left;">\n      <span>{{item}}</span>\n    </div>\n  </div>\n  <div>\n    <div col-4 class="num-keypad" style="float:left;">\n      <span>&nbsp;</span>\n    </div>\n  </div>\n  <div>\n    <div col-4 class="num-keypad" (click)="number(\'0\')" style="float:left;">\n      <span>0</span>\n    </div>\n  </div>\n  <div col-4 class="num-keypad" style="float:left;" (click)="cancel()">\n    <span text-center>\n      <ion-icon name="arrow-back" ></ion-icon>\n    </span>\n  </div>\n\n\n\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/password/password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ })

});
//# sourceMappingURL=5.js.map