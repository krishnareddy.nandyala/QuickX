webpackJsonp([10],{

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConformpasswordPageModule", function() { return ConformpasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__conformpassword__ = __webpack_require__(537);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConformpasswordPageModule = /** @class */ (function () {
    function ConformpasswordPageModule() {
    }
    ConformpasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__conformpassword__["a" /* ConformpasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__conformpassword__["a" /* ConformpasswordPage */]),
            ],
        })
    ], ConformpasswordPageModule);
    return ConformpasswordPageModule;
}());

//# sourceMappingURL=conformpassword.module.js.map

/***/ }),

/***/ 537:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConformpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConformpasswordPage = /** @class */ (function () {
    function ConformpasswordPage(navCtrl, navParams, toastCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.password = [];
        this.numbers = [1, 2, 3];
        this.numbers1 = [4, 5, 6];
        this.numbers2 = [7, 8, 9];
        this.numbers3 = ['', 0, ''];
        this.empty = ['', '', '', '', '', ''];
        this.password2 = [];
        this.lines = ['_', '_', '_', '_', '_', '_'];
        this.password = this.navParams.get("password");
    }
    ConformpasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConformpasswordPage');
    };
    ConformpasswordPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Password Mismatch',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ConformpasswordPage.prototype.number = function (num) {
        if (this.password2.length <= 5) {
            this.password2.push(num);
        }
        console.log("add", this.password2);
        if (this.password2.length == 6) {
            if (Array.prototype.toString.call(this.password) == Array.prototype.toString.call(this.password2)) {
                this.storage.set('passwords', this.password);
                this.navCtrl.setRoot("HomePage");
            }
            else {
                this.presentToast();
            }
        }
    };
    ConformpasswordPage.prototype.cancel = function () {
        this.password2.splice(this.password2.length - 1, 1);
        console.log("cancel", this.password2);
    };
    ConformpasswordPage.prototype.back = function () {
        this.navCtrl.setRoot('PasswordPage');
    };
    ConformpasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-conformpassword',template:/*ion-inline-start:"/home/krishna/krishna/projects/QuickX/src/pages/conformpassword/conformpassword.html"*/'<ion-content  style="background-repeat: no-repeat;\nbackground-size: 100% 100%;\nbackground-image:url(\'./assets/imgs/background.png\');">\n    \n<div class="close-btn" (click)=back()>X</div>\n  \n    <ion-row>\n      \n      <ion-col col-12 text-center>\n        <img class="img" src="./assets/imgs/quickx.png">\n      </ion-col>\n      \n    </ion-row>\n    <ion-row>\n      <ion-col col-12 text-center style="color: #fff; margin-top:5%;">\n        <h4 style="font-family: Poppins">Confirm Pin</h4>\n      </ion-col>\n    </ion-row>\n    <div>\n    <ion-row  style="height:38px;">\n  \n      <div *ngFor="let item of password2" >\n        <ion-col col-2 text-center style="padding:0px 5px; ">\n          <div class="fill" [ngStyle]="{\'background-color\':item ? \'white\' : \'\'}"></div>\n        </ion-col>\n      </div>\n    \n    </ion-row>\n  </div>\n    <ion-row >\n     \n        \n      <div *ngFor="let item of lines" style="text-align: center; ">\n        <ion-col col-2 >\n          <div  style="color:#fff; width: 17px; height: 17px;margin: 0 8px; text-align: center">{{item}}</div>\n        </ion-col>\n      </div>\n      \n      \n      \n    </ion-row>\n  \n  \n    <ion-row>\n     \n      <ion-col col-12 text-center style="color: lightgray; padding:0px;">\n        \n      </ion-col>\n      <ion-col col-3>\n  \n      </ion-col>\n    </ion-row>\n  \n  </ion-content>\n  <ion-footer>\n  \n    <div *ngFor="let item of numbers">\n      <div col-4 class="num-keypad" (click)="number(item)" style="float: left;">\n        <span>{{item}}</span>\n      </div>\n    </div>\n    <div *ngFor="let item of numbers1">\n      <div col-4 class="num-keypad" (click)="number(item)" style="float:left;">\n        <span>{{item}}</span>\n      </div>\n    </div>\n    <div *ngFor="let item of numbers2">\n      <div col-4 class="num-keypad" (click)="number(item)" style="float:left;">\n        <span>{{item}}</span>\n      </div>\n    </div>\n    <div>\n      <div col-4 class="num-keypad" style="float:left;">\n        <span>&nbsp;</span>\n      </div>\n    </div>\n    <div>\n      <div col-4 class="num-keypad" (click)="number(\'0\')" style="float:left;">\n        <span>0</span>\n      </div>\n    </div>\n    <div col-4 class="num-keypad" style="float:left;" (click)="cancel()">\n      <span  text-center>\n        <ion-icon name="arrow-back" ></ion-icon>\n      </span>\n    </div>\n  \n  \n  \n  </ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/QuickX/src/pages/conformpassword/conformpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], ConformpasswordPage);
    return ConformpasswordPage;
}());

//# sourceMappingURL=conformpassword.js.map

/***/ })

});
//# sourceMappingURL=10.js.map